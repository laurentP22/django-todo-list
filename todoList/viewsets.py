from rest_framework import viewsets
from .models import Todo
from .serializers import TodoSerializer


# ViewSet is like Resources or Controllers in other frameworks

class TodoViewSet(viewsets.ModelViewSet):
    queryset = Todo.objects.all()
    serializer_class = TodoSerializer