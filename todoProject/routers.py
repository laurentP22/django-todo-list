from rest_framework import routers
from todoList.viewsets import TodoViewSet

# Create or routes
# rest_framework will automaticaly create or routes:
# GET: /api/todoList/
# POST: /api/todoList
# DELETE: /api/todoList/{todo_id}/
# GET: /api/todoList/{todo_id}/
# PUT: /api/todoList/{todo_id}/


router = routers.DefaultRouter()

router.register(r'todoList', TodoViewSet)